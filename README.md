# Unlock hdd on boot using hdparm

Debian package to unlock hdd on boot using hdparm. You need to add device name to the file  /etc/dehddtab, then if device is locked, you will be prompted to enter the password on boot.